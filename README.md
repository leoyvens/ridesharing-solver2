# Repository structure

The `input` directory contains the inputs for the instances analyzed in the paper. Both the
instances in .lgf format in `input/raw`, and the pre-processed ones in `input/prepared` by running
this solver with the `--prepare` option.

The `output` directory contains a JSON file for each combination of instance type and auction, in
each file there is an array of JSON objects, each object corresponding to the collected results for
one instance. These results were analyzed utilizing the `notebook.ipynb` Jupyter Notebook file.

The solver itself is written in Rust. To compile and run it, first Rust must be installed. To run
the PLI solver, the GUROBI solver must also be installed. Make sure that the environment variable
`GUROBI_HOME` is set to the installation path of Gurobi (i.e. `/opt/gurobi652/linux64`).

The usage of the CLI is:
```
USAGE:
    ridesharing-solver [FLAGS] <n> --dev <dev>

FLAGS:
        --brute      Use the brute force solver. If not provided, the PLI solver is used.
        --force      If the output exists, write over it.
    -h, --help       Prints help information
        --prepare    Prepares input in input/raw and overwrites input/prepared.
    -V, --version    Prints version information

OPTIONS:
        --dev <dev>    Standard deviation of distribution.

ARGS:
    <n>    Number of passengers in instances.
```


For example, to solve the instance set of 10 passengers and standard deviation of bids of 3, using the brute force solver:
```
cargo run --release -- 10 --dev 3 --brute
```
