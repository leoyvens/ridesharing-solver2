use auction::{Auction, Cost};
use graph::Graph;
use model_graph::*;
use std::cmp::max;
use std::collections::HashMap;
use std::iter::FromIterator;
use PasVecExt;
use Passenger;
use Ride;

use crate::Solver;

// Caches solutions by size.
pub type SolutionCache = HashMap<u32, Ride>;

pub trait HashExt {
    fn insert_or_remove(&mut self, size: u32, ride: Option<Ride>);
    fn max_value_by<F: Fn(&Ride) -> i64>(&self, map: &F) -> Option<&Ride>;
}

impl HashExt for SolutionCache {
    fn insert_or_remove(&mut self, size: u32, ride: Option<Ride>) {
        if let Some(ride) = ride {
            println!("Replaced size {} with {}.", size, ride);
            self.insert(size, ride)
        } else {
            println!("No replacement for size {}.", size);
            self.remove(&size)
        };
    }

    fn max_value_by<F: Fn(&Ride) -> i64>(&self, map: &F) -> Option<&Ride> {
        // Sort by size and then compare by greater equal so that largest ride breaks tie.
        let mut key_value = Vec::from_iter(self.iter());
        key_value.sort_by_key(|&(size, _)| size);
        let mut values = key_value.iter().map(|&(_, ride)| ride);
        let mut max = values.next()?;
        for ride in values {
            if map(ride) >= map(max) {
                max = ride;
            }
        }
        Some(max)
    }
}

pub type SizeDepConstr = Box<dyn Fn(u32, &mut GraphModel) -> Constr>;

pub struct CachedGraphModel {
    pub graph_model: GraphModel,
    pub cache: SolutionCache,
    auct: Auction,

    // Empty if not FixedUniform.
    pub size_dep_constrs: Vec<SizeDepConstr>,

    // Only for fixed cost.
    fixed_min_surplus_var: Option<gurobi::Var>,
    max_size_for_pass: HashMap<Passenger, u32>,
}

impl Solver for CachedGraphModel {
    fn optimal(&mut self, optimize_cost: bool) -> Option<Ride> {
        self.optimal(optimize_cost)
    }

    fn update_cache_entry(&mut self, size: u32) {
        self.update_cache_entry(size)
    }

    fn forbid_superset(&mut self, passengers: Vec<Passenger>) -> Option<Constr> {
        Some(self.forbid_superset(passengers))
    }

    fn max_size_with(&mut self, pass: &Passenger, min_obj: u64) -> u32 {
        self.max_size_with(pass, min_obj)
    }
}

impl CachedGraphModel {
    pub fn new(graph: Graph, auct: Auction) -> Self {
        let graph_model = GraphModel::new(graph);
        let cache = SolutionCache::new();
        let mut me = CachedGraphModel {
            graph_model,
            cache,
            auct,
            size_dep_constrs: Vec::new(),
            fixed_min_surplus_var: None,
            max_size_for_pass: HashMap::new(),
        };
        if auct.cost.is_fixed() {
            me.fixed_min_surplus_var = Some(me.graph_model.add_fixed_min_surplus_var(auct.cost));
        }
        me.init_cache();

        // In fixed cost, this is redundant with positive min surplus, which is fine.
        let welfare = me.graph_model.virtual_welfare(auct.cost);
        me.graph_model
            .model
            .greater("positive welfare", welfare, 0.0);
        if auct.cost.is_fixed() {
            let real_cost = me.graph_model.path_cost();
            let fixed_cost = me.graph_model.sum_fixed_costs(auct.cost);
            me.graph_model
                .model
                .less("real_less_than_fixed", real_cost, fixed_cost);
        }
        me
    }

    /// Do not restrict by any constraint other than routing when calling this.
    /// This relies on the invariant that if the model becomes infeasible at size n,
    /// then it will be infeasible for any size > n.
    fn init_cache(&mut self) {
        let mut size = 1;
        while let Some(opt) = self.maximize_obj_for_size(size) {
            let obj_val = self.auct.obj_value(&opt);
            println!(
                "Found {:?} cost {} for size {} obj {}.",
                opt, opt.cost, size, obj_val
            );
            if obj_val >= 0 {
                if self.auct.cost == Cost::FixedUniform {
                    self.add_max_cost_constrs(&opt);
                }
                self.cache.insert(size, opt);
            } else if self.auct.cost.is_fixed() || matches!(self.auct.cost, Cost::Reserve) {
                panic!("negative surplus on fixed cost auction");
            }
            size += 1;

            // Limit max size.
            if size > 3 {
                break;
            }
        }
    }

    pub fn optimal(&mut self, optimize_cost: bool) -> Option<Ride> {
        // Pull best ride from cache.
        // If ride is still feasible, return it.
        // Otherwise, update it's entry and repeat.
        let auction = self.auct;
        while let Some(mut opt) = self
            .cache
            .max_value_by(&|ride| auction.obj_value(ride))
            .cloned()
        {
            let opt_size = opt.passengers.len() as u32;
            println!("checking if {} is still feasible", opt);
            let mut feasible;
            let mut require = self.graph_model.require_all(&opt.passengers);
            if !optimize_cost {
                feasible = self.for_size(opt_size, GraphModel::feasible);
                require.remove();
            } else {
                // Make sure ride has minimal cost.
                let cost = self.graph_model.path_cost();
                let auct_cost = self.auct.cost;
                let inner_opt = self.for_size(opt_size, |graph_model| {
                    graph_model.minimize(cost, auct_cost)
                });
                require.remove();
                feasible = inner_opt.is_some();
                // If feasible, update `opt`.
                if let Some(mut inner_opt) = inner_opt {
                    self.set_costs(&mut inner_opt);
                    opt = inner_opt;
                }
            }
            self.graph_model.forbid_ride(&opt);

            if auction.cost == Cost::FixedUniform && self.exists_cheaper(&opt) {
                feasible = false;
            }

            // Update max_size, infeasability is not an issue since it was once feasible.
            for pass in &opt.passengers {
                let max_size = self.max_size_for_pass.entry(*pass).or_default();
                *max_size = max(*max_size, opt.size() as u32);
            }

            if feasible {
                // Feasability does not depend on the bid, so only feasible rides may invalidate,
                // this needs test.
                if self.auct.cost == Cost::FixedUniform {
                    // Because we are forbidding opt, we should make sure more expensive rides
                    // are out since they would be invalidated by opt.
                    self.add_max_cost_constrs(&opt);
                }
                println!("Found opt {}.", opt);
                return Some(opt);
            } else {
                println!("{} is invalid", opt);
                self.update_cache_entry(opt.size());
            }
        }
        None
    }

    pub(crate) fn update_cache_entry(&mut self, size: u32) {
        // Redundant for fixed cost, which is fine.
        let min_surplus = self.min_surplus(size);
        let mut pos_surplus = self
            .graph_model
            .model
            .greater("positive surplus", min_surplus, 0.0);
        let ride = self.maximize_obj_for_size(size);
        match ride {
            Some(ref ride) if self.auct.cost == Cost::FixedUniform => {
                self.add_max_cost_constrs(ride)
            }
            _ => (),
        }
        pos_surplus.remove();
        self.cache.insert_or_remove(size, ride);
    }

    /// Returns a ride that makes the model feasible, `None` if always infeasible.
    pub(crate) fn feasible(&mut self) -> Option<Ride> {
        assert_eq!(self.auct.cost, Cost::FixedUniform);
        let keys = Vec::from_iter(self.cache.keys().cloned());
        for size in keys {
            // needs test that early return isn't the other way around.
            let auct_cost = self.auct.cost;
            if let Some(ride) =
                self.for_size(size, |model| model.maximize(LinExpr::new(), auct_cost))
            {
                return Some(ride);
            }
        }
        None
    }

    pub fn forbid_superset(&mut self, passengers: Vec<Passenger>) -> Constr {
        self.graph_model.forbid_superset(&passengers)
    }

    // Model state is the same before and after calling this.
    pub(crate) fn exists_cheaper(&mut self, ride: &Ride) -> bool {
        fn cheaper_cost_per_pass_constr(opt: &Ride) -> SizeDepConstr {
            let opt_cost = opt.cost_per_pass() as f64;
            Box::new(move |size, graph_model| {
                let cost = graph_model.path_cost() / size as f64;
                graph_model
                    .model
                    .less("cheaper_than_winner", cost, opt_cost - 1.0)
            })
        }

        let mut require_one = self.graph_model.require_one_or_more(&ride.passengers);
        self.size_dep_constrs
            .push(cheaper_cost_per_pass_constr(ride));
        // todo needs test, unused in current tests
        let cheaper = self.feasible();
        self.size_dep_constrs.pop();
        require_one.remove();
        if let Some(cheaper) = cheaper {
            // May want to do add constraints here to improve performance if getting this a lot.
            // TODO: this path needs test.
            println!("Exists cheaper {:?}.", cheaper);
            true
        } else {
            false
        }
    }

    pub fn max_size_with(&mut self, pass: &Passenger, min_obj: u64) -> u32 {
        let known_max_size_with = self.max_size_for_pass[pass];
        let global_max_size = *self.cache.keys().max().unwrap();

        // Must contain i
        let mut require = self.graph_model.require_all(&[*pass]);

        // Look for a size larger than the one currently known.
        for size in ((known_max_size_with + 1)..=global_max_size).rev() {
            println!("Looking for size {}", size);
            // Weighted minimum surplus must be at least min_obj
            let obj_expr = self.auct.obj_expr(size, self);
            let mut min_obj_constr =
                self.graph_model
                    .model
                    .greater("min_obj", obj_expr, min_obj as f64);
            let mut size_constr = self.graph_model.size_constr(size);
            let ride = self.graph_model.maximize(LinExpr::new(), self.auct.cost);

            // If the ride is viable
            if let Some(ride) = ride {
                println!(
                    "Found {} with value {}",
                    dbg!(&ride.passengers).iter().ids_string(),
                    self.auct.obj_value(&ride)
                );
                for p in ride.passengers {
                    let max_size = self.max_size_for_pass.get_mut(&p);
                    max_size.map(|max_size| *max_size = max(*max_size, size));
                }
            }
            size_constr.remove();
            min_obj_constr.remove();
        }
        require.remove();
        self.max_size_for_pass[pass]
    }

    fn maximize_obj_for_size(&mut self, size: u32) -> Option<Ride> {
        let auct_cost = self.auct.cost;
        let obj_expr = self.auct.obj_expr(size, self);
        let mut ride = self.for_size(size, |graph_model| {
            graph_model.maximize(obj_expr, auct_cost)
        });
        if let Some(ref mut ride) = ride {
            self.set_costs(ride);
        }
        ride
    }

    fn set_costs(&self, ride: &mut Ride) {
        // All should be unpriced, using to get &mut Passenger.
        let cost_per_pass = ride.cost_per_pass();
        for p in ride.unpriced() {
            p.cost = Some(self.auct.cost.for_pass(
                *p,
                Some(cost_per_pass),
                self.graph_model.roundtrip_cost(*p),
            ));
        }
    }

    pub(crate) fn min_surplus(&self, size: u32) -> LinExpr {
        use Cost::*;
        let model = &self.graph_model;
        match self.auct.cost {
            VariableUniform | FixedUniform => model.min_bid() - model.path_cost() / size as f64,
            Fixed(_) | FixedUpperBound => self.fixed_min_surplus_var.as_ref().unwrap().into(),
            ZeroPassengerCost | Reserve | ReserveUpperBound => model.min_bid().into(),
        }
    }

    fn add_max_cost_constrs(&mut self, ride: &Ride) {
        assert_eq!(self.auct.cost, Cost::FixedUniform);
        // todo: if opt is invalid, check if invalidator is valid.
        for p in &ride.passengers {
            let name = format!("max_cost_for_{}_from_{}", p.id, ride);
            let cost = self.graph_model.path_cost();
            let ride_cost = ride.cost_per_pass() as f64;
            let p_in = self.graph_model.sum_out_of_deliveries(&[*p]);
            let graph = self.graph_model.graph();
            let big_m = graph.edge_references().map(|e| e.weight().cost).sum();
            let new_constr = move |size, model: &mut GraphModel| {
                model.model.less(
                    &name,
                    cost.clone() / (size as f64),
                    ride_cost + (1.0 - p_in.clone()) * big_m,
                )
            };
            self.size_dep_constrs.push(Box::new(new_constr));
        }
    }

    fn for_size<T, F>(&mut self, size: u32, func: F) -> T
    where
        F: FnOnce(&mut GraphModel) -> T,
    {
        assert!(self.size_dep_constrs.is_empty() || self.auct.cost == Cost::FixedUniform);
        let mut size_constr = self.graph_model.size_constr(size);
        let mut tmp_constrs = Vec::new();
        for constr in &self.size_dep_constrs {
            tmp_constrs.push(constr(size, &mut self.graph_model));
        }
        let result = func(&mut self.graph_model);
        size_constr.remove();
        for mut constr in tmp_constrs {
            constr.remove();
        }
        result
    }
}

// Tests that the same ride can't be pulled twice from the cache.
#[test]
fn no_repeated_ride() {
    use auction;
    let graph = ::graph::example(0);
    let auction: Auction = (auction::MaxMinSurplus, auction::VariableUniform).into();
    let mut model = CachedGraphModel::new(graph, auction);
    let winner = model.optimal(false).unwrap();
    model.cache.insert(winner.size(), winner.clone());
    assert_ne!(winner, model.optimal(false).unwrap());
}

#[allow(non_snake_case)]
#[test]
fn lazy_replacement() {
    use auction;
    use graph::{self, Edge};
    let (mut G, S, E, P, D) = graph::with_bids_costs(0, &[1000, 400, 300], &[0, 2, 1]);
    G.extend_with_edges(&[
        (S, P[0], Edge::default()),
        (S, P[1], Edge::default()),
        (S, P[2], Edge::default()),
        (D[0], S, Edge::default()),
        (D[1], S, Edge::default()),
        (D[2], S, Edge::default()),
        (P[0], D[0], Edge::default()),
        (D[0], P[1], Edge::default()),
        (D[0], P[2], Edge::default()),
        (
            D[0],
            E,
            Edge {
                cost: 1000.0,
                seconds: 0,
            },
        ),
        (
            P[1],
            D[1],
            Edge {
                cost: 200.0,
                seconds: 0,
            },
        ),
        (
            P[2],
            D[2],
            Edge {
                cost: 100.0,
                seconds: 0,
            },
        ),
        (D[1], E, Edge::default()),
        (D[2], E, Edge::default()),
    ]);
    let auction: Auction = (auction::MaxMinSurplus, auction::VariableUniform).into();
    let mut cache = CachedGraphModel::new(G, auction);
    let opt = cache.optimal(false).unwrap();
    assert_eq!(opt, *cache.cache.get(&2).unwrap());
}
