extern crate petgraph;
extern crate rand;
extern crate serde;
#[macro_use]
extern crate serde_derive;
extern crate itertools;
extern crate rayon;
extern crate serde_json;
extern crate structopt;

mod auction;
mod brute_force;
mod cached_graph_model;
mod graph;
mod model_graph;
mod prob;
mod tests;

pub use auction::*;
use prob::*;
use rayon::prelude::*;
pub use std::fmt::{self, Debug, Display};
use std::fs::{create_dir, read_dir, File};
use std::hash::{Hash, Hasher};
pub use std::iter::FromIterator;
use structopt::StructOpt;

use crate::{brute_force::BruteForce, cached_graph_model::CachedGraphModel};

#[derive(Copy, Clone, Debug, Default, PartialEq, Eq, Serialize, Deserialize)]
pub struct Driver {
    arrive_until: u64,
    direct_cost: u64, // In cents.
}

#[derive(Copy, Clone, Debug, Default, Eq, Ord, PartialOrd, Serialize, Deserialize)]
pub struct Passenger {
    bid: u64, // In cents. Must be first field for `Ord` derivation.
    id: u64,
    price: Option<u64>, // In cents.
    cost: Option<u64>,
    direct_dist: u64, // In meters.
    direct_cost: u64, // In cents.
    direct_time: u64, // In seconds.
}

impl PartialEq for Passenger {
    fn eq(&self, other: &Passenger) -> bool {
        self.id == other.id
    }
}

impl Hash for Passenger {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.id.hash(state);
    }
}

impl Passenger {
    fn surplus(&self) -> i64 {
        let cost = self
            .cost
            .expect("Tried to get surplus without setting cost.");
        self.bid as i64 - cost as i64
    }
}

pub trait PasVecExt {
    fn ids(self) -> Vec<u64>;
    fn ids_string(self) -> String;
}

impl<Pass: std::ops::Deref<Target = Passenger>, I: Iterator<Item = Pass>> PasVecExt for I {
    fn ids(self) -> Vec<u64> {
        self.map(|p| p.id).collect()
    }
    fn ids_string(self) -> String {
        let ids = Vec::<String>::from_iter(self.ids().iter().map(u64::to_string));
        ids.join("_")
    }
}

#[derive(Serialize, Clone, Default, PartialEq, Eq)]
pub struct Ride {
    pub driver: Driver,
    pub passengers: Vec<Passenger>,
    pub cost: u64, // Cost is in cents, rounded up.
    pub virtual_welfare: i64,
}

impl Ride {
    pub fn cost_per_pass(&self) -> u64 {
        (self.cost as f64 / self.passengers.len() as f64).ceil() as u64
    }

    fn min_surplus(&self) -> i64 {
        self.passengers
            .iter()
            .map(Passenger::surplus)
            .min()
            .unwrap()
    }

    fn sum_bids(&self) -> u64 {
        self.passengers.iter().map(|p| p.bid).sum()
    }

    fn sum_costs(&self) -> u64 {
        self.passengers.iter().map(|p| p.cost.unwrap()).sum()
    }

    fn welfare(&self) -> i64 {
        self.sum_bids() as i64 - self.cost as i64
    }

    fn profit(&self) -> i64 {
        let price_sum: i64 = self
            .passengers
            .iter()
            .map(|p| p.price.unwrap_or(0) as i64)
            .sum();
        price_sum - self.cost as i64
    }

    /// Returns the unpriced passengers in `self` which are not in `other`.
    fn unpriced_complement(&mut self, other: &Ride) -> Vec<&mut Passenger> {
        self.passengers
            .iter_mut()
            .filter(|p| p.price.is_none() && !other.passengers.contains(p))
            .collect()
    }

    fn unpriced(&mut self) -> Vec<&mut Passenger> {
        self.passengers
            .iter_mut()
            .filter(|p| p.price.is_none())
            .collect()
    }

    fn size(&self) -> u32 {
        self.passengers.len() as u32
    }

    // Can be negative for VCG.
    fn cost_profit(&self) -> i64 {
        println!("self cost {}", self.cost);
        self.sum_costs() as i64 - self.cost as i64
    }
}

impl Debug for Ride {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.passengers.iter().ids_string())?;
        Ok(())
    }
}

impl Display for Ride {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut passengers = self.passengers.clone();
        passengers.sort();
        let costs: Vec<u64> = passengers.iter().map(|p| p.cost.unwrap()).collect();
        let surpluses: Vec<i64> = passengers.iter().map(Passenger::surplus).collect();
        write!(
            f,
            "{} Costs: {:?} Surpluses: {:?} vWelfare: {} Cost: {}",
            passengers.iter().ids_string(),
            costs,
            surpluses,
            self.virtual_welfare,
            self.cost
        )?;
        Ok(())
    }
}

#[derive(Debug, StructOpt)]
struct Cli {
    /// Number of passengers in instances.
    n: String,

    /// Prepares input in input/raw and overwrites input/prepared.
    #[structopt(long = "prepare")]
    prepare: bool,

    /// Standard deviation of distribution.
    #[structopt(long = "dev")]
    dev: u16,

    /// If the output exists, write over it.
    #[structopt(long = "force")]
    force: bool,

    /// Use the brute force solver. If not provided, the PLI solver is used.
    #[structopt(long = "brute")]
    brute: bool,
}

const PREPARED_DIR: &str = "input/prepared";
const CENTS_PER_METER: f64 = 0.03;

fn main() {
    let opts = Cli::from_args();
    if opts.prepare {
        prepare(&opts.n, opts.dev);
    } else {
        solve(&format!("{}_{}", opts.n, opts.dev), opts.force, opts.brute).unwrap();
    }
}

/// Pre-processes the input by generating bids.
fn prepare(dir: &str, dev: u16) {
    let distribution = HalfNormal {
        mean: CENTS_PER_METER,
        dev: dev as f64,
    };
    if read_dir(PREPARED_DIR).is_err() {
        create_dir(PREPARED_DIR).unwrap();
    }
    let prep_dir = format!("{}/{}_{}", PREPARED_DIR, dir, dev);
    println!("Writing prepared instances to {}", prep_dir);
    if read_dir(&prep_dir).is_err() {
        create_dir(&prep_dir).unwrap();
    }
    let paths = read_dir(format!("input/raw/{}", dir))
        .unwrap()
        .map(|f| f.unwrap().path())
        .filter(|p| p.extension().is_some());
    for path in paths {
        let name = path
            .file_stem()
            .unwrap()
            .to_string_lossy()
            .into_owned()
            .clone();
        // 0.03 is from Kleiner article.
        let graph = graph::parse(path.as_ref(), CENTS_PER_METER, &distribution).unwrap();
        // Write prepared graph.
        let file = File::create(format!("{}/{}.json", prep_dir, name)).unwrap();
        serde_json::to_writer_pretty(file, &graph).unwrap();
    }
}

fn solve(dir: &str, force: bool, brute: bool) -> Result<(), Box<dyn std::error::Error>> {
    use std::time::SystemTime;

    #[derive(Clone, Default, Serialize)]
    struct Run {
        graph_name: String,
        n_pass: u32,
        winning_ride: Ride,
        profit: i64,
        cost_profit: f64,
        welfare: u64,
        virtual_welfare: u64,
        running_time: u64,
    }

    impl Run {
        fn new(winner: Ride) -> Run {
            Run {
                n_pass: winner.passengers.len() as u32,
                profit: winner.profit(),
                welfare: winner.welfare() as u64,
                virtual_welfare: winner.virtual_welfare as u64,
                cost_profit: winner.cost_profit() as f64,
                winning_ride: winner,
                ..Run::default()
            }
        }
    }

    let input_dir = format!("{}/{}", PREPARED_DIR, dir);
    let output_dir = format!("output/{}", dir);
    if read_dir(&output_dir).is_err() {
        create_dir(&output_dir)?;
    }

    let paths = read_dir(input_dir)?
        .map(|f| f.unwrap().path())
        .filter(|p| p.extension().is_some());

    let mut graphs: Vec<(String, graph::Graph)> = paths
        .par_bridge()
        .map(|path| {
            let name = path
                .file_stem()
                .unwrap()
                .to_string_lossy()
                .into_owned()
                .clone();
            let graph = serde_json::from_reader(File::open(path).unwrap()).unwrap();
            println!("read {}", name);
            (name, graph)
        })
        .collect();
    graphs.sort_by_key(|(name, _)| name.clone());

    for &auction in &[
        // (Vcg, ZeroPassengerCost),
        // (Vcg, Fixed(100)),
        // (WeightedMaxMinSurplus, ZeroPassengerCost),
        // (WeightedMaxMinSurplus, Fixed(100)),
        // (Vcg, FixedUpperBound),
        // (VcgReserve, Reserve),
        // (VcgReserve, ReserveUpperBound),
        // (WeightedMaxMinSurplus, FixedUpperBound),
        (VcgReserve, ReserveUpperBound),
    ] {
        let mut runs = Vec::new();
        let auction = auction.into();
        let mut filename = format!("{}/{}", output_dir, auction);
        if brute {
            filename.push_str("_brute");
        }
        let output_path = format!("{}.json", filename);

        // If we're not forcing, don't re-calculate existing results.
        if !force && File::open(&output_path).is_ok() {
            println!("SKIP {}", auction);
            continue;
        }

        for &(ref name, ref g) in &graphs {
            println!("SOLVING {} {}", name, auction);

            let start = SystemTime::now();
            let winner = if brute {
                let mut solver = BruteForce::new(g.clone(), auction);
                auction::solve(&mut solver, auction)
            } else {
                let mut solver = CachedGraphModel::new(g.clone(), auction);
                auction::solve(&mut solver, auction)
            };
            let mut run = Run::new(winner.unwrap_or_default());
            if auction.cost == ZeroPassengerCost {
                run.cost_profit = 0.0;
            }
            run.graph_name = name.clone();
            run.running_time = SystemTime::now().duration_since(start)?.as_millis() as u64 / 10;
            println!("Winner: {}", run.winning_ride);
            println!("Profit: {}", run.profit);
            println!(
                "Prices: {:?}",
                Vec::from_iter(run.winning_ride.passengers.iter().map(|p| p.price))
            );
            runs.push(run);
        }

        let file = File::create(output_path).unwrap();
        serde_json::to_writer_pretty(file, &runs).unwrap();
    }
    Ok(())
}
