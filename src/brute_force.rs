use std::{collections::BTreeMap, usize};

use crate::{cached_graph_model::SolutionCache, graph::GraphExt, Kind, Solver};
use auction::Auction;
use graph::Graph;
use itertools::Itertools;
use model_graph::*;
use petgraph::prelude::NodeIndex;
use rayon::prelude::*;
use PasVecExt;
use Passenger;
use Ride;

#[derive(Copy, Clone)]
pub struct GraphPassenger {
    pickup: NodeIndex,
    delivery: NodeIndex,
    passenger: Passenger,
}

#[derive(Copy, Clone)]
enum PermNode {
    Pickup(GraphPassenger),
    Delivery(GraphPassenger),
}

impl PermNode {
    fn idx(&self) -> NodeIndex {
        match self {
            Self::Pickup(p) => p.pickup,
            Self::Delivery(p) => p.delivery,
        }
    }

    fn is_pickup(&self) -> bool {
        match self {
            Self::Pickup(_) => true,
            Self::Delivery(_) => false,
        }
    }

    fn passenger(&self) -> Passenger {
        match self {
            Self::Pickup(p) => p.passenger,
            Self::Delivery(p) => p.passenger,
        }
    }
}

enum Combinations {
    Unoptimized(Vec<Vec<GraphPassenger>>),
    Optimized(Vec<Ride>),
}

pub struct BruteForce {
    pub g: Graph,
    pub cache: SolutionCache,
    auct: Auction,
    combinations: Combinations,
    start_idx: usize,
    max_size_for_pass: BTreeMap<Passenger, u32>,
}

impl Solver for BruteForce {
    // Cost is always optimized in brute force.
    fn optimal(&mut self, _optimize_cost: bool) -> Option<Ride> {
        self.optimal()
    }

    // Noop.
    fn update_cache_entry(&mut self, _size: u32) {}

    // Not helpful for brute force, so we just let it be optimized until all passengers are priced.
    fn forbid_superset(&mut self, _passengers: Vec<Passenger>) -> Option<Constr> {
        None
    }

    // This is always precomputed in brute force, because all alternatives until wm*_-i are evaluated.
    fn max_size_with(&mut self, pass: &Passenger, _min_obj: u64) -> u32 {
        self.max_size_for_pass[pass]
    }
}

impl BruteForce {
    pub fn new(g: Graph, auct: Auction) -> Self {
        let cache = SolutionCache::new();

        let passengers: Vec<_> = g
            .passenger_pickups()
            .iter_mut()
            .map(|(idx, p)| {
                p.cost = Some(auct.cost.for_pass(*p, None, g.roundtrip_cost(*p)));
                GraphPassenger {
                    pickup: *idx,
                    delivery: g.delivery(*p),
                    passenger: *p,
                }
            })
            .collect();

        let combinations = passengers
            .iter()
            .cloned()
            .combinations(1)
            .chain(passengers.iter().cloned().combinations(2))
            .chain(passengers.iter().cloned().combinations(3))
            .par_bridge();

        let combinations = match auct.kind {
            Kind::WeightedMaxMinSurplus => {
                let mut combinations: Vec<_> = combinations
                    .filter(|combination| {
                        // check min surplus.
                        combination
                            .iter()
                            .map(|p| p.passenger.bid as i64 - p.passenger.cost.unwrap() as i64)
                            .min()
                            .unwrap()
                            >= 0
                    })
                    .collect();
                combinations.sort_by_cached_key(|c| {
                    (c.len() as u64)
                        * c.iter()
                            .map(|&p| p.passenger.bid - p.passenger.cost.unwrap())
                            .min()
                            .unwrap()
                });
                combinations.reverse();
                Combinations::Unoptimized(combinations)
            }
            Kind::Vcg | Kind::VcgReserve => {
                let mut combinations: Vec<_> = combinations
                    .filter_map(|c| optimal_permutation(&g, auct, &c))
                    .collect();
                combinations.sort_by_cached_key(|ride| auct.obj_value(&ride));
                combinations.reverse();
                Combinations::Optimized(combinations)
            }
            _ => unimplemented!(),
        };

        BruteForce {
            g,
            cache,
            auct,
            combinations,
            start_idx: 0,
            max_size_for_pass: BTreeMap::new(),
        }
    }

    pub fn optimal(&mut self) -> Option<Ride> {
        let ride = self.maximize_obj()?;
        for pass in &ride.passengers {
            let max_size = self.max_size_for_pass.entry(*pass).or_default();
            *max_size = (*max_size).max(ride.size() as u32);
        }

        println!("Found brute opt {}.", ride);
        Some(ride)
    }

    fn maximize_obj(&mut self) -> Option<Ride> {
        match &self.combinations {
            Combinations::Unoptimized(c) => {
                match c
                    .iter()
                    .skip(self.start_idx)
                    .enumerate()
                    .find_map(|(idx, combination)| {
                        optimal_permutation(&self.g, self.auct, combination).map(|ride| (idx, ride))
                    }) {
                    Some((optimal_idx, optimal)) => {
                        self.start_idx += optimal_idx + 1;
                        Some(optimal)
                    }
                    None => {
                        self.start_idx = c.len();
                        None
                    }
                }
            }
            Combinations::Optimized(c) => {
                let res = c.iter().skip(self.start_idx).next().cloned();
                self.start_idx += 1;
                res
            }
        }
    }
}

fn optimal_permutation(
    g: &Graph,
    auct: Auction,
    combination: &Vec<GraphPassenger>,
) -> Option<Ride> {
    let driver_start = g.driver_start().0;
    let driver_max_time = g.driver_start().1.arrive_until as u32;
    let driver_end = g.driver_end();

    let all_nodes = combination
        .iter()
        .map(|&pass| vec![PermNode::Pickup(pass), PermNode::Delivery(pass)])
        .flatten();

    let route_cost = all_nodes
        .permutations(combination.len() * 2)
        .filter_map(|perm| {
            // validate pickup/delivery order
            {
                let mut seen_pickups = vec![];
                for &n in &perm {
                    match n {
                        PermNode::Pickup(p) => seen_pickups.push(p.pickup),
                        PermNode::Delivery(p) => {
                            if !seen_pickups.contains(&p.pickup) {
                                return None;
                            }
                        }
                    }
                }
            }

            let mut cost = 0.0;
            let mut time: u64 = 0;
            let mut passenger_start_time = BTreeMap::<Passenger, u64>::new();

            let start_edge = g[g.find_edge(driver_start, perm[0].idx()).unwrap()];
            cost += start_edge.cost;
            time += start_edge.seconds as u64;
            assert!(perm[0].is_pickup());
            passenger_start_time.insert(perm[0].passenger(), time);

            // We start with 1 passenger in the car.
            let mut in_car = 1;

            for (t, u) in perm.iter().tuple_windows() {
                let edge = g[g.find_edge(t.idx(), u.idx())?];
                cost += edge.cost;
                time += edge.seconds as u64;
                if u.is_pickup() {
                    in_car += 1;
                    if in_car > 2 {
                        return None;
                    }
                    // Pickup in at most 15 minutes from start.
                    if time > 60 * 15 {
                        // println!(
                        //     "invalid {} due to pickup time",
                        //     combination.iter().map(|p| &p.passenger).ids_string()
                        // );
                        return None;
                    }
                    passenger_start_time.insert(u.passenger(), time);
                } else {
                    in_car -= 1;
                    if (time - passenger_start_time[&u.passenger()]) > 2 * u.passenger().direct_time
                    {
                        // println!(
                        //     "invalid {} due to trip time",
                        //     combination.iter().map(|p| &p.passenger).ids_string()
                        // );
                        return None;
                    }
                }
            }

            let end_edge = g[g.find_edge(perm.last().unwrap().idx(), driver_end).unwrap()];
            cost += end_edge.cost;
            time += end_edge.seconds as u64;

            if time > driver_max_time as u64 {
                // println!(
                //     "invalid {} due to driver max time",
                //     combination.iter().map(|p| &p.passenger).ids_string()
                // );
                return None;
            }

            Some(cost.ceil() as u64)
        })
        .min()?;

    // println!(
    //     "Found permutation for {}",
    //     combination.iter().map(|p| &p.passenger).ids_string()
    // );

    let real_cost = (route_cost as i64 - g.driver_start().1.direct_cost as i64).max(0) as u64;
    let total_bids: u64 = combination.iter().map(|p| p.passenger.bid).sum();

    if auct.cost.is_fixed() {
        let total_cost: u64 = combination.iter().map(|&p| p.passenger.cost.unwrap()).sum();
        if total_cost < real_cost {
            // println!(
            //     "invalid {} due real cost {} is less than {}",
            //     combination.iter().map(|p| &p.passenger).ids_string(),
            //     total_cost,
            //     real_cost
            // );
            return None;
        }
    }

    let virtual_welfare = if auct.cost.is_fixed() {
        total_bids as i64
            - combination
                .iter()
                .map(|&p| p.passenger.cost.unwrap())
                .sum::<u64>() as i64
    } else {
        total_bids as i64 - real_cost as i64
    };

    let ride = Ride {
        virtual_welfare,
        passengers: combination.iter().map(|&p| p.passenger).collect(),
        cost: real_cost,
        driver: g.driver_start().1,
    };

    Some(ride)
}
