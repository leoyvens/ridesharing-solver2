pub use self::Cost::*;
pub use self::Kind::*;
use cached_graph_model::*;
use model_graph::gurobi::Constr;
use model_graph::LinExpr;
use *;

#[derive(Copy, Clone, Debug, Eq, PartialEq, Serialize)]
pub enum Cost {
    VariableUniform,
    Fixed(u16),      // Cost ratio, in percentage points.
    FixedUpperBound, // b_i >= c_i implies viable ride.
    FixedUniform,
    ZeroPassengerCost,
    Reserve, // The reserve cost is assumed to be the direct cost.
    ReserveUpperBound,
}

impl Cost {
    pub fn for_pass(
        self,
        p: Passenger,
        cost_per_pass: Option<u64>,
        roundtrip_cost: Option<u64>,
    ) -> u64 {
        match self {
            VariableUniform | FixedUniform => cost_per_pass.unwrap(),
            Fixed(ratio) => ((ratio as f64 / 100.0) * p.direct_cost as f64) as u64,
            Reserve => p.direct_cost,
            ReserveUpperBound => roundtrip_cost.unwrap(),
            FixedUpperBound => roundtrip_cost.unwrap(),
            ZeroPassengerCost => 0,
        }
    }

    pub fn is_fixed(self) -> bool {
        match self {
            Fixed(_) | FixedUpperBound => true,
            _ => false,
        }
    }
}

impl Display for Cost {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        Debug::fmt(self, f)
    }
}

#[derive(Copy, Clone, Debug, Eq, PartialEq, Serialize)]
pub enum Kind {
    Vcg,
    VcgReserve,
    MaxMinSurplus,
    WeightedMaxMinSurplus,
}

impl Display for Kind {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        Debug::fmt(self, f)
    }
}

#[derive(Copy, Clone, Debug, Eq, PartialEq, Serialize)]
pub struct Auction {
    pub kind: Kind,
    pub cost: Cost,
}

impl Display for Auction {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        Ok(write!(f, "{:?}_{:?}", self.kind, self.cost)?)
    }
}

impl From<(Kind, Cost)> for Auction {
    fn from((kind, cost): (Kind, Cost)) -> Auction {
        Auction { kind, cost }
    }
}

impl Auction {
    // Objective of auction. Sense of expr assumed to be maximize.
    pub fn obj_expr(self, size: u32, model: &CachedGraphModel) -> LinExpr {
        match self.kind {
            MaxMinSurplus => model.min_surplus(size),
            WeightedMaxMinSurplus => size as f64 * model.min_surplus(size),

            // `cost` is ignored here because `Vcg` and `Reserve` are not `is_fixed()`.
            Vcg | VcgReserve => model.graph_model.virtual_welfare(self.cost),
        }
    }

    pub fn obj_value(self, ride: &Ride) -> i64 {
        match self.kind {
            MaxMinSurplus => ride.min_surplus(),
            WeightedMaxMinSurplus => ride.size() as i64 * ride.min_surplus(),

            // This is sum of values minus driver cost.
            Vcg | VcgReserve => ride.virtual_welfare,
        }
    }
}
pub trait Solver {
    fn optimal(&mut self, optimize_cost: bool) -> Option<Ride>;
    fn update_cache_entry(&mut self, size: u32);
    fn forbid_superset(&mut self, passengers: Vec<Passenger>) -> Option<Constr>;
    fn max_size_with(&mut self, pass: &Passenger, min_obj: u64) -> u32;
}

pub(super) fn solve(solver: &mut dyn Solver, auct: Auction) -> Option<Ride> {
    let mut winner = solver.optimal(auct.cost.is_fixed())?;

    solver.update_cache_entry(winner.size());
    let winner_value = auct.obj_value(&winner);

    println!("Found winner {} with value {}.", winner, winner_value);
    let mut no_superset_constr = solver.forbid_superset(winner.passengers.clone());

    // While there are feasible rides, find prices for i in winner.
    while let Some(opt) = solver.optimal(false) {
        if let Some(mut no_superset_constr) = no_superset_constr {
            no_superset_constr.remove();
        }

        // Calculate price for all p in winner such that opt = A*₋ᵢ
        for p in winner.unpriced_complement(&opt) {
            let cost = p.cost.unwrap();
            let price = match auct.kind {
                MaxMinSurplus => cost + opt.min_surplus() as u64,
                WeightedMaxMinSurplus => {
                    let opt_value = auct.obj_value(&opt) as u64;
                    cost + (opt_value / solver.max_size_with(p, opt_value) as u64)
                }
                Vcg => {
                    // winner - p might not exist because of cost > real. So force 0 in that case.
                    cost + (auct.obj_value(&opt) + p.surplus() - winner_value).max(0) as u64
                }
                VcgReserve => {
                    // winner - p might not exist because of cost > real. So force 0 in that case.
                    let clarke_price = (auct.obj_value(&opt) + p.bid as i64 - winner_value).max(0);
                    cost.max(clarke_price as u64)
                }
            };
            p.price = Some(price);
            println!("Priced {} with {}", p.id, price);
        }

        let unpriced = winner.unpriced();
        no_superset_constr = match unpriced.is_empty() {
            false => solver.forbid_superset(unpriced.into_iter().map(|&mut p| p).collect()),
            true => break,
        };

        solver.update_cache_entry(opt.size());
    }

    // Price unpriced passengers with cost.
    for p in winner.unpriced() {
        let cost = p.cost.unwrap();
        println!("pricing {} with cost {:?}.", p.id, cost);
        p.price = Some(cost);
    }

    Some(winner)
}

// Tests that lowering the bid can't make a passenger be served by invalidating
// the ride that would cause an ordering violation.
#[test]
fn cant_invalidate_invalidator() {
    use graph::Node;
    let mut graph = ::graph::example(0);
    let winner = solve((MaxMinSurplus, FixedUniform).into(), graph.clone()).unwrap();
    assert!(!winner.passengers.iter().ids().contains(&5));
    // 5 tries to invalidate 1_2_3, invalidating the invalidator.
    for p in graph
        .node_weights_mut()
        .filter_map(Node::passenger_mut)
        .filter(|p| p.id == 5)
    {
        p.bid -= 31;
    }
    let winner2 = solve((MaxMinSurplus, FixedUniform).into(), graph).unwrap();
    assert!(!winner2.passengers.iter().ids().contains(&5));
}
