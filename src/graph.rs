use petgraph;

pub use self::Node::*;
pub use petgraph::graph::{EdgeIndex, NodeIndex};
pub use petgraph::Direction;
pub use petgraph::Direction::*;
pub use std::path::PathBuf;

use super::Driver;
use super::Passenger;

use std::fmt::{self, Display};

use std::collections::HashMap;
use std::error::Error;
use std::fs::File;
use std::io::Read;
use std::iter::FromIterator;
use std::path::Path;

use rand::distributions::Distribution;

pub type Graph = petgraph::Graph<Node, Edge>;

#[derive(Copy, Clone, Debug, PartialEq, Serialize, Deserialize)]
pub enum Node {
    Pickup(Passenger),
    Delivery(Passenger),
    DriverStart(Driver),
    DriverEnd(Driver),
}

impl Node {
    pub fn driver_start(&self) -> Option<Driver> {
        match *self {
            DriverStart(driver) => Some(driver),
            _ => None,
        }
    }

    #[cfg(test)]
    pub fn passenger_mut(&mut self) -> Option<&mut Passenger> {
        match *self {
            Pickup(ref mut p) | Delivery(ref mut p) => Some(p),
            _ => None,
        }
    }

    pub fn passenger(&self) -> Option<Passenger> {
        match *self {
            Pickup(p) | Delivery(p) => Some(p),
            _ => None,
        }
    }

    pub fn pickup(&self) -> Option<Passenger> {
        match *self {
            Pickup(p) => Some(p),
            _ => None,
        }
    }

    pub fn delivery(&self) -> Option<Passenger> {
        match *self {
            Delivery(p) => Some(p),
            _ => None,
        }
    }

    pub fn is_delivery(&self) -> bool {
        if let Delivery(_) = *self {
            true
        } else {
            false
        }
    }

    pub fn is_pickup(&self) -> bool {
        if let Pickup(_) = *self {
            true
        } else {
            false
        }
    }

    pub(crate) fn capacity(&self) -> i8 {
        if self.is_pickup() {
            1
        } else if self.is_delivery() {
            -1
        } else {
            // Driver node.
            0
        }
    }
}

impl Default for Node {
    fn default() -> Node {
        panic!("Node has no default.");
    }
}

impl Display for Node {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Pickup(pass) => write!(f, "P{}", pass.id),
            Delivery(pass) => write!(f, "D{}", pass.id),
            DriverStart(_) => write!(f, "Start"),
            DriverEnd(_) => write!(f, "End"),
        }
    }
}

#[derive(Copy, Clone, Default, Serialize, Deserialize)]
pub struct Edge {
    pub cost: f64,
    pub seconds: u32,
}

impl Display for Edge {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.cost)
    }
}

pub trait GraphExt {
    fn driver_start(&self) -> (NodeIndex, Driver);
    fn driver_end(&self) -> NodeIndex;
    fn passengers(&self) -> Vec<(NodeIndex, Passenger)>;
    fn deliveries(&self) -> Vec<(NodeIndex, Passenger)>;
    fn delivery(&self, pickup: Passenger) -> NodeIndex;
    fn pickups(&self) -> Vec<NodeIndex>;
    fn delivery_idx(&self, idx: NodeIndex) -> NodeIndex;
    fn deliveries_idx(&self) -> Vec<NodeIndex>;
    fn deliveries_pass(&self) -> Vec<Passenger>;
    fn max_bid(&self) -> u64;
    fn roundtrip_cost(&self, p: Passenger) -> Option<u64>;
    fn passenger_for_id(&self, id: u64) -> Passenger;
    fn passenger_pickups(&self) -> Vec<(NodeIndex, Passenger)>;
}

impl GraphExt for Graph {
    fn driver_start(&self) -> (NodeIndex, Driver) {
        self.node_indices()
            .filter_map(|idx| self[idx].driver_start().and_then(|d| Some((idx, d))))
            .nth(0)
            .unwrap()
    }

    fn driver_end(&self) -> NodeIndex {
        self.node_indices()
            .find(|node| {
                if let DriverEnd(_) = self[*node] {
                    true
                } else {
                    false
                }
            })
            .unwrap()
    }

    fn passengers(&self) -> Vec<(NodeIndex, Passenger)> {
        self.node_indices()
            .filter_map(|node| self[node].passenger().and_then(|p| Some((node, p))))
            .collect()
    }

    fn deliveries(&self) -> Vec<(NodeIndex, Passenger)> {
        self.passengers()
            .into_iter()
            .filter(|&(idx, _)| self[idx].is_delivery())
            .collect()
    }

    fn deliveries_idx(&self) -> Vec<NodeIndex> {
        self.deliveries().into_iter().map(|(idx, _)| idx).collect()
    }

    fn deliveries_pass(&self) -> Vec<Passenger> {
        self.passengers().into_iter().map(|(_, p)| p).collect()
    }

    fn pickups(&self) -> Vec<NodeIndex> {
        self.passengers()
            .into_iter()
            .filter_map(|(idx, _)| self[idx].pickup().and(Some(idx)))
            .collect()
    }

    fn passenger_pickups(&self) -> Vec<(NodeIndex, Passenger)> {
        self.passengers()
            .into_iter()
            .filter(|&(idx, _)| self[idx].pickup().is_some())
            .collect()
    }

    fn delivery(&self, p: Passenger) -> NodeIndex {
        self.deliveries()
            .into_iter()
            .find(|&(_, delivery)| p.id == delivery.id)
            .unwrap()
            .0
    }

    fn delivery_idx(&self, idx: NodeIndex) -> NodeIndex {
        let pickup = self[idx]
            .pickup()
            .expect("Looked for delivery of non-pickup");
        self.deliveries()
            .into_iter()
            .find(|&(_, delivery)| pickup.id == delivery.id)
            .unwrap()
            .0
    }

    fn max_bid(&self) -> u64 {
        self.deliveries()
            .into_iter()
            .map(|(_, pass)| pass.bid)
            .max()
            .unwrap()
    }

    fn passenger_for_id(&self, id: u64) -> Passenger {
        *self.deliveries_pass().iter().find(|p| p.id == id).unwrap()
    }

    fn roundtrip_cost(&self, p: Passenger) -> Option<u64> {
        let pickup = self
            .pickups()
            .into_iter()
            .find(|&pickup| p.id == self[pickup].pickup().unwrap().id)
            .unwrap();

        let driver_start = self.driver_start().0;
        let pickup_cost = self[self.find_edge(driver_start, pickup)?].cost.ceil() as u64;
        let return_cost = self[self.find_edge(self.delivery(p), driver_start)?]
            .cost
            .ceil() as u64;
        let start_roundtrip = pickup_cost + p.direct_cost + return_cost;

        let driver_end = self.driver_end();
        let end_pickup_cost = self[self.find_edge(driver_end, pickup)?].cost.ceil() as u64;
        let end_return_cost = self[self.find_edge(self.delivery(p), driver_end)?]
            .cost
            .ceil() as u64;
        let end_roundtrip = end_pickup_cost + p.direct_cost + end_return_cost;

        Some(start_roundtrip.min(end_roundtrip))
    }
}

pub fn parse<D: Distribution<f64>>(
    path: &Path,
    cost_per_meter: f64,
    distribution: &D,
) -> Result<Graph, Box<dyn Error>> {
    let mut graph = Graph::new();
    let mut lgf = String::new();
    File::open(path)?.read_to_string(&mut lgf)?;
    // Skip @nodes and nodes header.
    let mut lines = lgf.lines().skip(2).map(str::trim);
    // File id to graph id map.
    let mut graph_id: HashMap<u64, u32> = HashMap::new();
    // Parse nodes
    while let Some(line) = lines.next() {
        if line == "@arcs" {
            break;
        }
        let mut line = line.split_whitespace();
        let file_id = line.next().unwrap().parse()?;
        // Split properties by /
        let prop = Vec::<&str>::from_iter(line.next().unwrap().split('/'));
        let id = prop[0].parse()?;
        let is_pickup = prop[1].parse::<u8>()? == 1;
        if is_pickup {
            continue;
        } // We only need the delivery.
        let is_driver = prop[2].parse::<u8>()? == 1;
        let direct_time = prop[3].parse()?;
        let direct_dist = prop[4].parse()?;
        let direct_cost = (direct_dist as f64 * cost_per_meter).ceil() as u64;
        if is_driver {
            let driver = Driver {
                arrive_until: 2400,
                direct_cost,
            };
            // Depends on delivery immediately after pickup on file.
            graph_id.insert(
                file_id - 1,
                graph.add_node(DriverStart(driver)).index() as u32,
            );
            graph_id.insert(file_id, graph.add_node(DriverEnd(driver)).index() as u32);
        } else {
            let pass = Passenger {
                bid: generate_bid(direct_dist, distribution),
                id,
                direct_dist,
                direct_cost,
                direct_time,
                ..Passenger::default()
            };

            graph_id.insert(file_id, graph.add_node(Delivery(pass)).index() as u32);
            // Depends on delivery immediately before pickup on file.
            graph_id.insert(file_id + 1, graph.add_node(Pickup(pass)).index() as u32);
        }
    }
    // Skip arcs header, parse arcs.
    for mut line in lines.skip(1).map(str::split_whitespace) {
        let s = line.next().unwrap().parse()?;
        let t = line.next().unwrap().parse()?;
        // Skip label, split properties by /
        let mut prop = line.nth(1).unwrap().split('/');
        let dist: f64 = prop.next().unwrap().parse()?;
        let seconds = prop.next().unwrap().parse()?;
        graph.add_edge(
            graph_id[&s].into(),
            graph_id[&t].into(),
            Edge {
                cost: dist * cost_per_meter,
                seconds,
            },
        );
    }
    Ok(graph)
}

/// Generate bid as half gaussian between 0.3 and 3 per km (values from Kleiner article).
/// Used sigmas: 1, 2, 5, 10.
fn generate_bid<D: Distribution<f64>>(direct_dist: u64, distribution: &D) -> u64 {
    let direct_dist = direct_dist as f64;
    // Distribution must handle the minimum.
    let factor = distribution.sample(&mut ::rand::thread_rng()).min(3.0);
    // div by 10 since we would divide dist by 1000 (to km) and mul total by 100 (to cents).
    (factor * direct_dist / 10.0) as u64
}

#[allow(non_snake_case)]
#[cfg(test)]
pub fn with_bids_costs(
    direct_cost: u64,
    bids: &[u64],
    cost: &[u64],
) -> (Graph, NodeIndex, NodeIndex, Vec<NodeIndex>, Vec<NodeIndex>) {
    let mut G = Graph::new();
    let mut P = Vec::<NodeIndex>::new();
    let mut D = Vec::<NodeIndex>::new();
    // Driver nodes.
    let driver = Driver {
        arrive_until: 0,
        direct_cost,
    };
    let S = G.add_node(DriverStart(driver));
    let E = G.add_node(DriverEnd(driver));

    for (id, (&bid, &direct_cost)) in bids.iter().zip(cost.iter()).enumerate() {
        let id = id as u64;
        let passenger = Passenger {
            id,
            bid,
            direct_cost,
            ..Passenger::default()
        };
        P.push(G.add_node(Pickup(passenger)));
        D.push(G.add_node(Delivery(passenger)));
    }
    (G, S, E, P, D)
}

#[allow(non_snake_case)]
#[cfg(test)]
pub fn example(direct_cost: u64) -> Graph {
    let (mut G, S, E, P, D) = with_bids_costs(
        direct_cost,
        &[40, 50, 60, 50, 80, 60],
        &[10, 20, 10, 20, 20, 20],
    );

    G.extend_with_edges(&[
        // Start-pickup edges.
        (
            S,
            P[0],
            Edge {
                cost: 20.0,
                seconds: 0,
            },
        ),
        (
            S,
            P[1],
            Edge {
                cost: 30.0,
                seconds: 0,
            },
        ),
        (
            S,
            P[2],
            Edge {
                cost: 50.0,
                seconds: 0,
            },
        ),
        (
            S,
            P[3],
            Edge {
                cost: 10.0,
                seconds: 0,
            },
        ),
        (
            S,
            P[4],
            Edge {
                cost: 20.0,
                seconds: 0,
            },
        ),
        (
            S,
            P[5],
            Edge {
                cost: 20.0,
                seconds: 0,
            },
        ),
        // Delivery-start edges,
        (
            D[0],
            S,
            Edge {
                cost: 20.0,
                seconds: 0,
            },
        ),
        (
            D[1],
            S,
            Edge {
                cost: 30.0,
                seconds: 0,
            },
        ),
        (
            D[2],
            S,
            Edge {
                cost: 50.0,
                seconds: 0,
            },
        ),
        (
            D[3],
            S,
            Edge {
                cost: 10.0,
                seconds: 0,
            },
        ),
        (
            D[4],
            S,
            Edge {
                cost: 20.0,
                seconds: 0,
            },
        ),
        (
            D[5],
            S,
            Edge {
                cost: 20.0,
                seconds: 0,
            },
        ),
        //Other edges.
        (
            P[0],
            D[0],
            Edge {
                cost: 10.0,
                seconds: 0,
            },
        ),
        (
            P[4],
            P[2],
            Edge {
                cost: 20.0,
                seconds: 0,
            },
        ),
        (
            P[4],
            D[4],
            Edge {
                cost: 20.0,
                seconds: 0,
            },
        ),
        (
            P[3],
            D[3],
            Edge {
                cost: 20.0,
                seconds: 0,
            },
        ),
        (
            P[5],
            D[5],
            Edge {
                cost: 20.0,
                seconds: 0,
            },
        ),
        (
            D[0],
            P[1],
            Edge {
                cost: 10.0,
                seconds: 0,
            },
        ),
        (
            D[3],
            D[4],
            Edge {
                cost: 20.0,
                seconds: 0,
            },
        ),
        (
            P[2],
            D[2],
            Edge {
                cost: 10.0,
                seconds: 0,
            },
        ),
        (
            D[3],
            E,
            Edge {
                cost: 30.0,
                seconds: 0,
            },
        ),
        (
            P[1],
            P[2],
            Edge {
                cost: 20.0,
                seconds: 0,
            },
        ),
        (
            D[2],
            D[4],
            Edge {
                cost: 10.0,
                seconds: 0,
            },
        ),
        (
            P[1],
            D[1],
            Edge {
                cost: 20.0,
                seconds: 0,
            },
        ),
        (
            D[2],
            D[1],
            Edge {
                cost: 10.0,
                seconds: 0,
            },
        ),
        (
            D[1],
            E,
            Edge {
                cost: 10.0,
                seconds: 0,
            },
        ),
        (
            D[2],
            E,
            Edge {
                cost: 100.0,
                seconds: 0,
            },
        ),
        (
            D[4],
            E,
            Edge {
                cost: 20.0,
                seconds: 0,
            },
        ),
        (
            D[5],
            E,
            Edge {
                cost: 30.0,
                seconds: 0,
            },
        ),
        (
            P[4],
            P[3],
            Edge {
                cost: 10.0,
                seconds: 0,
            },
        ),
    ]);

    G
}
