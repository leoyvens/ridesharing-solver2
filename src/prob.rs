pub use rand::distributions::{Distribution, Normal};

pub struct HalfNormal {
    pub mean: f64,
    pub dev: f64,
}

impl Distribution<f64> for HalfNormal {
    fn sample<R: ::rand::Rng + ?Sized>(&self, rng: &mut R) -> f64 {
        let normal_sample = Normal::new(0.0, self.dev).sample(rng);
        self.mean + normal_sample.abs()
    }
}
