pub extern crate gurobi;

pub use self::gurobi::ConstrSense::*;
pub use self::gurobi::ModelSense::*;
pub use self::gurobi::{Constr, LinExpr, Model};

use self::gurobi::attr::DoubleAttr;
use self::gurobi::param::{DoubleParam, IntParam};
use self::gurobi::Status::*;
use self::gurobi::{Env, Var};
use super::Passenger;
use super::Ride;
use auction::Cost;
use graph::*;
use std::collections::HashMap;
use PasVecExt;

pub trait ModelExt {
    fn add_binary(&mut self, name: &str) -> Result<Var, gurobi::Error>;
    fn add_pos_real(&mut self, name: &str, upper: f64) -> Result<Var, gurobi::Error>;
    fn add_real(&mut self, name: &str, lower: f64, upper: f64) -> Result<Var, gurobi::Error>;
    fn less<T: AsRef<str>, S: Into<LinExpr>, B: Into<LinExpr>>(
        &mut self,
        name: T,
        small: S,
        big: B,
    ) -> Constr;
    fn greater<T: AsRef<str>, S: Into<LinExpr>, B: Into<LinExpr>>(
        &mut self,
        name: T,
        big: S,
        small: B,
    ) -> Constr;
    fn var_value(&self, var: &Var) -> f64;
    fn obj_value(&self) -> f64;
}

impl ModelExt for Model {
    fn add_binary(&mut self, name: &str) -> Result<Var, gurobi::Error> {
        self.add_var(name, gurobi::Binary, 0.0, 0.0, 1.0, &[], &[])
    }

    fn add_real(&mut self, name: &str, lower: f64, upper: f64) -> Result<Var, gurobi::Error> {
        self.add_var(name, gurobi::Continuous, 0.0, lower, upper, &[], &[])
    }

    fn add_pos_real(&mut self, name: &str, upper: f64) -> Result<Var, gurobi::Error> {
        self.add_var(name, gurobi::Continuous, 0.0, 0.0, upper, &[], &[])
    }

    fn less<T: AsRef<str>, S: Into<LinExpr>, B: Into<LinExpr>>(
        &mut self,
        name: T,
        small: S,
        big: B,
    ) -> Constr {
        self.add_constr(name.as_ref(), small.into() - big.into(), Less, 0.0)
            .unwrap()
    }

    fn greater<T: AsRef<str>, S: Into<LinExpr>, B: Into<LinExpr>>(
        &mut self,
        name: T,
        big: S,
        small: B,
    ) -> Constr {
        self.add_constr(name.as_ref(), big.into() - small.into(), Greater, 0.0)
            .unwrap()
    }

    fn var_value(&self, var: &Var) -> f64 {
        var.get(self, DoubleAttr::X).unwrap()
    }

    fn obj_value(&self) -> f64 {
        self.get(DoubleAttr::ObjVal).unwrap()
    }
}

pub struct GraphModel {
    g: Graph,
    pub model: Model,
    #[allow(dead_code)]
    env: Env,
    // Edge vars.
    e: HashMap<EdgeIndex, Var>,
    // Time vars.
    b: HashMap<NodeIndex, Var>,
    // Capacity vars.
    q: HashMap<NodeIndex, Var>,
    min_bid: Var,
}

impl GraphModel {
    pub fn new(g: Graph) -> GraphModel {
        let mut env = Env::new("gurobi_log.txt").unwrap();
        env.set(IntParam::LogToConsole, 0).unwrap();
        // Tuning showed these parameters help.
        env.set(DoubleParam::Heuristics, 0.0).unwrap();
        env.set(IntParam::VarBranch, 1).unwrap();
        env.set(IntParam::CutPasses, 1).unwrap();
        let mut model = Model::new("Model", &env).unwrap();

        // Edge variables.
        let mut edges = HashMap::<EdgeIndex, Var>::new();
        for e in g.edge_indices() {
            let (s, t) = g.edge_endpoints(e).unwrap();
            edges.insert(e, model.add_binary(&format!("{}-{}", g[s], g[t])).unwrap());
        }
        // Min bid var. Not always necessary, hopefully doesn't harm models where it is unused.
        // Consider extracting to cached_graph_model
        let min_bid = model.add_pos_real("min_bid", g.max_bid() as f64).unwrap();
        model.update().unwrap();
        let mut me = GraphModel {
            g,
            model,
            e: edges,
            b: HashMap::new(),
            q: HashMap::new(),
            env,
            min_bid,
        };
        me.add_min_bid_constrs();
        me.add_driver_flow_constrs();
        me.add_passengers_flow_constrs().unwrap();
        me.add_time_constrs().unwrap();
        me.add_capacity_constrs().unwrap();
        me
    }

    pub fn graph(&self) -> &Graph {
        &self.g
    }
    #[allow(dead_code)]
    pub fn expr_value(&self, expr: LinExpr) -> f64 {
        expr.get_value(&self.model).unwrap()
    }

    pub fn add_min_bid_constrs(&mut self) {
        for (idx, pass) in self.g.deliveries() {
            let name = &format!("min_bid_{}", self.g[idx]);
            // Max bid serves as big M.
            let max_bid = self.g.max_bid() as f64;
            let p_not_in_ride = 1.0 - self.edges_sum(idx, Outgoing);
            self.model.less(
                name,
                self.min_bid.clone(),
                pass.bid as f64 + max_bid * p_not_in_ride,
            );
        }
    }

    pub fn add_time_constrs(&mut self) -> gurobi::Result<()> {
        let driver_max = self.g.driver_start().1.arrive_until;
        // Time variables.
        for idx in self.g.node_indices() {
            let node = self.g[idx];
            // Pickup in at most 15 minutes from start. Respect driver_max.
            let max = if node.is_pickup() {
                60 * 15
            } else {
                driver_max
            };
            self.b.insert(
                idx,
                self.model
                    .add_pos_real(&format!("arrival-{:?}", node), max as f64)?,
            );
        }
        self.model.update()?;
        for pick in self.g.pickups() {
            let pass = self.g[pick].passenger().unwrap();
            let del = self.g.delivery_idx(pick);
            // Max travel time is two times direct time.
            let max_travel_time = 2.0 * pass.direct_time as f64;
            let travel_time = &self.b[&del] - &self.b[&pick];
            // Respect passenger time.
            self.model.less(
                &format!("travel_time-{}", pass.id),
                travel_time.clone(),
                max_travel_time,
            );
            // Precedence constraint.
            self.model
                .greater(&format!("precedence-{}", pass.id), travel_time, 0.0);
        }
        // Set arrival times. Unbounded waiting is implicit here.
        for e in self.g.edge_indices() {
            let (s, t) = self.g.edge_endpoints(e).unwrap();
            let big_m = 2.0 * driver_max as f64;
            self.model.less(
                &format!("time-{}-{}", self.g[s], self.g[t]),
                &self.b[&s] + self.g[e].seconds as f64 - big_m * (1.0 - &self.e[&e]),
                &self.b[&t],
            );
        }
        Ok(())
    }

    pub fn add_capacity_constrs(&mut self) -> gurobi::Result<()> {
        // Capacity variables, respect maximum capaicty.
        for idx in self.g.node_indices() {
            let node = self.g[idx];
            self.q.insert(
                idx,
                self.model
                    .add_pos_real(&format!("capacity-{:?}", node), 2.0)?,
            );
        }
        self.model.update()?;
        // Set capacity when leaving node.
        for e in self.g.edge_indices() {
            let (s, t) = self.g.edge_endpoints(e).unwrap();
            let big_m = self.g.deliveries().len() as f64;
            self.model.less(
                &format!("time-{}-{}", self.g[s], self.g[t]),
                &self.q[&s] + self.g[t].capacity() as f64 - big_m * (1.0 - &self.e[&e]),
                &self.q[&t],
            );
        }
        Ok(())
    }

    pub fn roundtrip_cost(&self, p: Passenger) -> Option<u64> {
        self.g.roundtrip_cost(p)
    }

    fn add_driver_flow_constrs(&mut self) {
        let driver_start_outgoing = self.edges_sum(self.g.driver_start().0, Outgoing);
        self.model
            .add_constr(
                "1_edge_leaves_driver_start",
                driver_start_outgoing,
                Equal,
                1.0,
            )
            .unwrap();
        let driver_end_incoming = self.edges_sum(self.g.driver_end(), Incoming);
        self.model
            .add_constr("1_edge_enters_driver_end", driver_end_incoming, Equal, 1.0)
            .unwrap();
    }

    fn add_passengers_flow_constrs(&mut self) -> gurobi::Result<()> {
        for (pass_idx, pass) in self.g.passengers() {
            let incoming = self.edges_sum(pass_idx, Incoming);
            let outgoing = self.edges_sum(pass_idx, Outgoing);
            let node = self.g[pass_idx];
            let _ = self.model.add_constr(
                &format!("incoming_=_outgoing_{}", node),
                incoming.clone() - outgoing,
                Equal,
                0.0,
            )?;

            if node.is_pickup() {
                self.model.add_constr(
                    &format!("pass_picked_most_once_{}", node),
                    incoming.clone(),
                    Less,
                    1.0,
                )?;
                let delivery_out = self.edges_sum(self.g.delivery(pass), Outgoing);
                self.model.add_constr(
                    &format!("picked_pass_delivered_{}", node),
                    incoming - delivery_out,
                    Equal,
                    0.0,
                )?;
            }
        }
        Ok(())
    }

    pub fn path_cost(&self) -> LinExpr {
        let total_dist = self
            .g
            .edge_indices()
            .map(|edge| self.g[edge].cost * &self.e[&edge])
            .sum::<LinExpr>();
        total_dist - self.g.driver_start().1.direct_cost as f64
    }

    pub fn feasible(&mut self) -> bool {
        self.maximize(LinExpr::new(), Cost::ZeroPassengerCost);
        match self.model.status().unwrap() {
            Optimal => true,
            Infeasible | InfOrUnbd => false,
            _ => unimplemented!(),
        }
    }

    pub fn minimize(&mut self, objective: LinExpr, cost: Cost) -> Option<Ride> {
        self.model.update().unwrap();
        self.model.set_objective(objective, Minimize).unwrap();
        self.optimize(cost)
    }

    pub fn maximize(&mut self, objective: LinExpr, cost: Cost) -> Option<Ride> {
        self.model.update().unwrap();
        self.model.set_objective(objective, Maximize).unwrap();
        self.optimize(cost)
    }

    pub fn forbid_ride(&mut self, ride: &Ride) {
        let ride_passengers = self.sum_out_of_deliveries(&ride.passengers);
        let non_ride_passengers =
            self.sum_out_edges(self.g.deliveries_idx().into_iter()) - ride_passengers.clone();
        let ride_len = ride.passengers.len() as f64;
        // or ride_passengers <= ride_len - 1 or non_ride_passengers >= 1
        let big_m = self.g.node_count() as f64;
        let name = format!("forbid_ride_{}", ride.passengers.iter().ids_string());
        let or = self.model.add_binary(&(name.clone() + "_or_var")).unwrap();
        self.model.update().unwrap();
        self.model.less(
            name.clone() + "-1",
            ride_passengers,
            ride_len - 1.0 + &or * big_m,
        );
        self.model.greater(
            name.clone() + "-2",
            non_ride_passengers,
            1.0 + (LinExpr::from(or) - 1.0) * big_m,
        );
    }

    pub fn require_one_or_more(&mut self, passengers: &Vec<Passenger>) -> Constr {
        let all_outgoing = self.sum_out_of_deliveries(passengers);
        self.model
            .add_constr("require_one_or_more", all_outgoing, Greater, 1.0)
            .unwrap()
    }

    pub fn require_all(&mut self, passengers: &[Passenger]) -> Constr {
        let all_outgoing = self.sum_out_of_deliveries(passengers);
        self.model
            .add_constr("require_all", all_outgoing, Equal, passengers.len() as f64)
            .unwrap()
    }

    pub fn forbid_superset(&mut self, passengers: &Vec<Passenger>) -> Constr {
        let n_passengers = passengers.len() as f64;
        let all_outgoing = self.sum_out_of_deliveries(passengers);
        self.model
            .add_constr("forbid_superset", all_outgoing, Less, n_passengers - 1.0)
            .unwrap()
    }

    #[allow(dead_code)]
    pub fn write_iss_and_panic(&mut self) -> ! {
        self.model.compute_iis().unwrap();
        self.model.write("output/ISS.ilp").unwrap();
        panic!("ISS written.")
    }

    pub fn size_constr(&mut self, size: u32) -> Constr {
        let all_edges = self.all_edges();
        self.model
            .add_constr("ride_size", all_edges, Equal, (size as f64) * 2.0 + 1.0)
            .unwrap()
    }

    pub fn max_size_constr(&mut self, size: u32) -> Constr {
        let all_edges = self.all_edges();
        self.model
            .less("ride_size", all_edges, (size as f64) * 2.0 + 1.0)
    }

    pub fn sum_out_of_deliveries(&self, passengers: &[Passenger]) -> LinExpr {
        self.sum_out_edges(passengers.iter().map(|p| self.g.delivery(*p)))
    }

    fn sum_out_edges<I: Iterator<Item = NodeIndex>>(&self, nodes: I) -> LinExpr {
        nodes.map(|idx| self.edges_sum(idx, Outgoing)).sum()
    }

    pub fn add_fixed_min_surplus_var(&mut self, cost: Cost) -> Var {
        let var = self
            .model
            .add_pos_real("fixed_min_surplus", self.g.max_bid() as f64)
            .unwrap();
        // Max bid serves as big M. Multiply by two to avoid issues with fractional variables.
        let max_bid = 2.0 * self.g.max_bid() as f64;
        for (idx, p) in self.g.deliveries() {
            let p_indicator = self.edges_sum(idx, Outgoing);
            let name = format!("fixed_min_surplus_{}", p.id);
            // Being extra careful here about overflow.
            let surplus: i64 =
                p.bid as i64 - cost.for_pass(p, None, self.g.roundtrip_cost(p)) as i64;
            self.model
                .less(&name, &var, surplus as f64 + max_bid * (1.0 - p_indicator));
        }
        var
    }

    pub fn min_bid(&self) -> &Var {
        &self.min_bid
    }

    pub(crate) fn sum_fixed_costs(&self, cost: Cost) -> LinExpr {
        let passengers = self.g.deliveries();
        passengers.iter().fold(LinExpr::new(), |acc, &(idx, p)| {
            acc + self.edges_sum(idx, Outgoing)
                * cost.for_pass(p, None, self.g.roundtrip_cost(p)) as f64
        })
    }

    pub(crate) fn virtual_welfare(&self, cost: Cost) -> LinExpr {
        let virtual_cost = if cost.is_fixed() {
            self.sum_fixed_costs(cost)
        } else {
            self.path_cost()
        };
        self.sum_bids() - virtual_cost
    }

    fn optimize(&mut self, cost: Cost) -> Option<Ride> {
        self.model.optimize().unwrap();
        match self.model.status().unwrap() {
            Optimal => Some(self.solution_ride(cost)),
            Infeasible | InfOrUnbd => None,
            _ => unimplemented!(),
        }
    }

    fn solution_ride(&self, auct_cost: Cost) -> Ride {
        // For whatever reason sometimes the path is slightly shorter than the driver's direct distance.
        let path_cost = self.expr_value(self.path_cost()).ceil();
        let cost = if path_cost > -50.0 {
            path_cost.max(0.0) as u64
        } else {
            panic!("metric violatiion, cost {}", path_cost)
        };
        let virtual_welfare = self.expr_value(self.virtual_welfare(auct_cost)) as i64;
        Ride {
            passengers: self.passengers_in_sol(),
            cost,
            virtual_welfare,
            driver: self.g.driver_start().1,
        }
    }

    fn passengers_in_sol(&self) -> Vec<Passenger> {
        let mut passengers: Vec<Passenger> = Vec::new();
        for (idx, passenger) in self.g.deliveries() {
            if self
                .edges_sum(idx, Outgoing)
                .get_value(&self.model)
                .unwrap()
                > 0.9
            {
                passengers.push(passenger);
            }
        }
        passengers
    }

    fn all_edges(&self) -> LinExpr {
        self.g
            .edge_indices()
            .map(|edge| &self.e[&edge])
            .cloned()
            .map(Into::into)
            .sum()
    }

    pub(crate) fn sum_bids(&self) -> LinExpr {
        let passengers = self.g.deliveries();
        passengers.iter().fold(LinExpr::new(), |acc, &(idx, p)| {
            acc + self.edges_sum(idx, Outgoing) * p.bid as f64
        })
    }

    fn edges_sum(&self, idx: NodeIndex, dir: Direction) -> LinExpr {
        let mut edges = self.g.neighbors_directed(idx, dir).detach();
        let mut sum = LinExpr::new();
        while let Some(edge) = edges.next_edge(&self.g) {
            sum += self.e[&edge].clone();
        }
        sum
    }
}

#[test]
fn forbid_ride() {
    // Test that ride is forbidden even even different paths serving the same ride exist.
    use petgraph::visit::EdgeRef;
    let mut graph = ::graph::example(0);
    // Duplicate edges.
    for edge in graph.clone().edge_references() {
        graph.add_edge(edge.source(), edge.target(), *edge.weight());
    }
    let mut model = GraphModel::new(graph);
    model.size_constr(3); // To ensure unique solution.
    let ride = model
        .maximize(LinExpr::new(), Cost::ZeroPassengerCost)
        .unwrap();
    model.forbid_ride(&ride);
    assert!(!model.feasible());

    // Test that forbid ride doesn't forbid supersets.
    graph = ::graph::example(0);
    model = GraphModel::new(graph);
    // Forbid all size 2 rides.
    let mut size_constr = model.size_constr(2);
    while let Some(ride) = model.maximize(LinExpr::new(), Cost::ZeroPassengerCost) {
        model.forbid_ride(&ride);
    }
    size_constr.remove();
    model.size_constr(3);
    assert!(model.feasible(), "forbid ride shouldn't forbid supersets");
}
