pub use self::Cost::*;
pub use self::Kind::*;
use *;

// broken because example doesn't have the edges to calculate upper bound.
#[ignore]
#[test]
fn example_fixed_upper_bound() {
    let winner = auction::solve(
        (WeightedMaxMinSurplus, FixedUpperBound).into(),
        ::graph::example(60),
    )
    .unwrap();
    assert_eq!(
        Vec::from_iter(winner.passengers.iter().map(|p| p.id)),
        vec![3, 4]
    );
    assert_eq!(winner.profit(), 25 + 60);
}

#[test]
fn example_weighted_max_min_surplus_fixed() {
    use std::iter::FromIterator;
    let winner = auction::solve(
        (WeightedMaxMinSurplus, Fixed(100)).into(),
        ::graph::example(60),
    )
    .unwrap();
    println!("{}", winner);
    assert_eq!(
        Vec::from_iter(winner.passengers.iter().map(|p| p.id)),
        vec![2, 4]
    );
    assert_eq!(winner.size() * winner.min_surplus() as u32, 100);
    assert_eq!(winner.profit(), 75);
}

#[test]
fn non_cached_max_size() {
    use graph::Node;
    use std::iter::FromIterator;

    let mut graph = ::graph::example(60);
    for p2_node in graph
        .node_weights_mut()
        .filter_map(Node::passenger_mut)
        .filter(|p| p.id == 2)
    {
        p2_node.bid = 300; // high bid
    }
    let winner = auction::solve((WeightedMaxMinSurplus, ZeroPassengerCost).into(), graph).unwrap();
    println!("{}", winner);
    assert_eq!(
        Vec::from_iter(winner.passengers.iter().map(|p| p.id)),
        vec![2]
    );
    assert_eq!(winner.size() * winner.min_surplus() as u32, 300);
    assert_eq!(winner.profit(), -67);
}

#[test]
fn example_weighted_max_min_surplus_variable() {
    use std::iter::FromIterator;
    let winner = auction::solve(
        (WeightedMaxMinSurplus, VariableUniform).into(),
        ::graph::example(60),
    )
    .unwrap();
    println!("{}", winner);
    assert_eq!(
        Vec::from_iter(winner.passengers.iter().map(|p| p.id)),
        vec![2, 4]
    );
    assert_eq!(winner.size() * winner.min_surplus() as u32, 40 + 60);
    assert_eq!(winner.profit(), 71);
}

#[test]
fn example_weighted_max_min_surplus_fixed_uniform() {
    use std::iter::FromIterator;
    let winner = auction::solve(
        (WeightedMaxMinSurplus, FixedUniform).into(),
        ::graph::example(60),
    )
    .unwrap();
    println!("{}", winner);
    assert_eq!(
        Vec::from_iter(winner.passengers.iter().map(|p| p.id)),
        vec![4]
    );
    assert_eq!(winner.size() * winner.min_surplus() as u32, 80);
    assert_eq!(winner.profit(), 50);
}

#[test]
fn example_weighted_max_min_surplus_fixed_uniform_driver_zero() {
    use std::iter::FromIterator;
    let winner = auction::solve(
        (WeightedMaxMinSurplus, FixedUniform).into(),
        ::graph::example(0),
    )
    .unwrap();
    println!("{}", winner);
    assert_eq!(
        Vec::from_iter(winner.passengers.iter().map(|p| p.id)),
        vec![0, 1, 2]
    );
    assert_eq!(winner.size() * winner.min_surplus() as u32, 30);
    // The profit should be 7.5 by pricing all with 4_5, but 4_5 is improperly
    // invalidated by 3_5 which is itself invalidated by 1_2_3, which has neither 4 nor 5.
    assert_eq!(winner.profit(), 0);
}

#[test]
fn example_max_min_surplus_variable() {
    use std::iter::FromIterator;
    let winner = auction::solve(
        (MaxMinSurplus, VariableUniform).into(),
        ::graph::example(60),
    )
    .unwrap();
    assert_eq!(
        Vec::from_iter(winner.passengers.iter().map(|p| p.id)),
        vec![4]
    );
    assert_eq!(winner.cost, 0);
    assert_eq!(winner.min_surplus(), 80);
    assert_eq!(winner.profit(), 50);
}

// Also tests that ties are broken by largest ride.
#[test]
fn example_max_min_surplus_variable_driver_zero() {
    use std::iter::FromIterator;
    let winner =
        auction::solve((MaxMinSurplus, VariableUniform).into(), ::graph::example(0)).unwrap();
    assert_eq!(
        Vec::from_iter(winner.passengers.iter().map(|p| p.id)),
        vec![2, 4]
    );
    assert_eq!(winner.cost, 80);
    assert_eq!(winner.min_surplus(), 20);
    assert_eq!(winner.profit(), 30);
}

#[test]
fn example_max_min_surplus_fixed_uniform_driver_zero() {
    use std::iter::FromIterator;
    let winner = auction::solve((MaxMinSurplus, FixedUniform).into(), ::graph::example(0)).unwrap();
    assert_eq!(
        Vec::from_iter(winner.passengers.iter().map(|p| p.id)),
        vec![0, 1, 2]
    );
    assert_eq!(winner.cost, 90);
    assert_eq!(winner.min_surplus(), 10);
    // The profit should be 15 by pricing all with 4_5, but 4_5 is improperly
    // invalidated by 3_5 which is itself invalidated by 1_2_3, which has neither 4 nor 5.
    assert_eq!(winner.profit(), 0);
}

#[test]
fn example_max_min_surplus_fixed_uniform() {
    use std::iter::FromIterator;
    let winner =
        auction::solve((MaxMinSurplus, FixedUniform).into(), ::graph::example(60)).unwrap();
    assert_eq!(
        Vec::from_iter(winner.passengers.iter().map(|p| p.id)),
        vec![4]
    );
    assert_eq!(winner.cost, 0);
    assert_eq!(winner.min_surplus(), 80);
    assert_eq!(winner.profit(), 50);
}

#[test]
fn example_pure_vcg() {
    let winner = auction::solve((Vcg, ZeroPassengerCost).into(), ::graph::example(60)).unwrap();
    assert_eq!(winner.virtual_welfare, 120);
    assert_eq!(winner.profit(), 100);
}

#[test]
fn example_vcg_fixed() {
    let winner = auction::solve((Vcg, Fixed(100)).into(), ::graph::example(60)).unwrap();
    assert_eq!(winner.virtual_welfare, 110);
    assert_eq!(winner.profit(), 100);
}
